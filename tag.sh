#!/bin/sh
set -e

if [ "$#" -lt 4 ]; then
  echo >&2 'Insufficient number of argments provided!'
  exit 1
fi

base_image_tag=${1}
ref_tag=${2}
ref_commit_tag=${3}
full_version=${4}


docker pull ${ref_commit_tag}

# Is this a stable or RC?
if echo $full_version | grep -Eq '^(0|[1-9][\d]*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-rc\.[1-9]\d*)?$';
then

    echo "Stable or RC!"

    docker tag ${ref_commit_tag} ${base_image_tag}:${full_version}
    docker push ${base_image_tag}:${full_version}

    # Is this a stable release?
    if echo $full_version | grep -Eq '^(0|[1-9][\d]*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)$';
    then

        echo "Definitely stable!"

        major_only_ver=$(echo $full_version | cut -d '.' -f 1)
        major_minor_ver="${major_only_ver}.$(echo $full_version | cut -d '.' -f 2)"

        echo $major_only_ver
        echo $major_minor_ver

        docker tag ${ref_commit_tag} "${base_image_tag}:${major_only_ver}"
        docker push "${base_image_tag}:${major_only_ver}"

        docker tag ${ref_commit_tag} "${base_image_tag}:${major_minor_ver}"
        docker push "${base_image_tag}:${major_minor_ver}"   

        # TODO Check if latest actually IS the latest
        docker tag ${ref_commit_tag} ${base_image_tag}:latest
        docker push ${base_image_tag}:latest

    fi

# Not a stable or RC commit
else
    
    docker tag ${ref_commit_tag} ${ref_tag}:latest
    docker push ${ref_tag}:latest

fi

