#!/bin/sh
set -e

if [ "$#" -lt 1 ]; then
  echo >&2 'JDK version must be provided as the first positional argument'
  exit 1
fi

jdk_version=${1}

if [ "${CI}" == "true" ]; then

       commit_time=${CI_COMMIT_TIMESTAMP}
       # Keep everything but the time zone designator (position 20+), remove all "-" and ":", and replace the time designator ("T") with a "_"
       # Format: YYYYMMDD_HHMMSS
       compact_commit_time=$(echo "${commit_time}" | cut -c "1-19" | sed -e "s/-//g" -e "s/://g" -e "s/T/_/g")
       ref_slug=${CI_COMMIT_REF_SLUG}
       commit_short_sha=${CI_COMMIT_SHORT_SHA}
       version=${CI_COMMIT_TAG}
       project_url=${CI_PROJECT_URL}
       base_image_title=${CI_PROJECT_TITLE}
       project_registry_address=${CI_REGISTRY_IMAGE}

else

       if ! command -v git &> /dev/null; then
              echo "git command could not be found"
              exit
       fi

       commit_time=$(git show -s --format=%ci)
       compact_commit_time=$(echo "${commit_time}" | cut -c "1-19" | sed -e "s/-//g" -e "s/://g" -e "s/ /_/g")
       ref_slug=$(git rev-parse --abbrev-ref HEAD | iconv -t ascii//TRANSLIT | sed -e s/[^a-zA-Z0-9]+/-/g -e s/^-+\|-+$//g | tr A-Z a-z)
       commit_short_sha=$(git show -s --format=%H | cut -c "1-8" )
       version="WIP"
       project_url=$(git remote get-url origin | sed -e 's/git@/https:\/\//g' -e 's/\.git//g' -e 's/.com\:/.com\//g')
       base_image_title=$(basename $(pwd))
       project_registry_address=$(echo "${project_url}" | sed -e 's/https:\/\//registry\./g')

fi

source_url="${project_url}.git"

image_variant="${jdk_version}-jdk-linux"
#Just the title of the image, excluding registry address
full_image_title="${base_image_title}/${image_variant}"

base_image_tag="${project_registry_address}/${image_variant}"
ref_tag="${base_image_tag}/${ref_slug}"
ref_commit_tag="${ref_tag}:${compact_commit_time}-${commit_short_sha}"

# CI doesn't preserve env vars between jobs. A .env file can be used by donwstream jobs to reference the tag info
if [ "${CI}" == "true" ]; then

       dot_env_filename="tag-${jdk_version}-jdk.env"

       echo "BASE_IMAGE_TAG=${base_image_tag}" >> ${dot_env_filename}       
       echo "REF_TAG=${ref_tag}" >> ${dot_env_filename}       
       echo "REF_COMMIT_TAG=${ref_commit_tag}" >> ${dot_env_filename}

fi

echo "Building image and tagging as ${ref_commit_tag}"

docker build \
       --pull \
       -t ${ref_commit_tag} \
       --build-arg JDK_VERSION=${jdk_version} \
       --label "org.opencontainers.image.created=${commit_time}" \
       --label "org.opencontainers.image.ref.name=${ref_slug}" \
       --label "org.opencontainers.image.revision=${commit_short_sha}" \
       --label "org.opencontainers.image.source=${source_url}" \
       --label "org.opencontainers.image.title=${full_image_title}" \
       --label "org.opencontainers.image.url=${project_url}" \
       --label "org.opencontainers.image.version=${version}" \
       .
