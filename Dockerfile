FROM almalinux:8.7-minimal-20230407

ARG JDK_VERSION

LABEL org.opencontainers.image.base.name="almalinux:8.7-minimal-20230407" \
      org.opencontainers.image.base.digest="sha256:f019b7981a104a4a6328ff2701c746fdd4b8a7f1f3f0dbb767e4c40cf02dd79e" \
      org.opencontainers.image.description="Docker container to support development of JDK based projects" \
      org.opencontainers.image.licenses="CC0-1.0" \ 
      org.opencontainers.image.vendor="US DOT, FAA, Office of NextGen" 

# "Base" layer(s)
RUN microdnf upgrade && \
    microdnf install --nodocs glibc-langpack-en && \
    microdnf clean all && \ 
    rm -rf /var/cache/yum

ENV LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_ALL=en_US.UTF-8

# SSH layer(s)
RUN microdnf upgrade && \
    microdnf install --nodocs openssh-clients sshpass && \
    microdnf clean all && \ 
    rm -rf /var/cache/yum

RUN mkdir -p /root/.ssh && \
    chmod 700 /root/.ssh

# JDK Layer(s)
ADD includes/adoptium.repo /etc/yum.repos.d/adoptium.repo

ENV JAVA_HOME=/usr/lib/jvm/temurin-${JDK_VERSION}-jdk/

# findutils includes xargs, which Gradle needs
RUN microdnf install --nodocs findutils temurin-${JDK_VERSION}-jdk && \
    microdnf clean all && \ 
    rm -rf /var/cache/yum && \
    rm -rf ${JAVA_HOME}/src.zip && \
    rm -rf ${JAVA_HOME}/man && \
    rm -rf ${JAVA_HOME}/sample
