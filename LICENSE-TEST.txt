3rd Party Licenses for test

1. Group: com.beust  Name: jcommander  Version: 1.78

POM Project URL: https://jcommander.org

POM License: Apache License, Version 2.0 - http://www.apache.org/licenses/LICENSE-2.0.txt

--------------------------------------------------------------------------------

2. Group: com.github.javaparser  Name: javaparser-core  Version: 3.24.0

Manifest Project URL: https://github.com/javaparser/javaparser-core

Manifest License: "GNU Lesser General Public License";link="http://www.gnu.org/licenses/lgpl-3.0.html","Apache License, Version 2.0";link="http://www.apache.org/licenses/LICENSE-2.0.txt";description="A business-friendly OSS license" (Not packaged)

POM License: Apache License, Version 2.0 - http://www.apache.org/licenses/LICENSE-2.0.txt

POM License: GNU Lesser General Public License - http://www.gnu.org/licenses/lgpl-3.0.html
